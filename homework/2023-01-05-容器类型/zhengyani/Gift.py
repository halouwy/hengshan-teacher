'''
@data:2023/1/6
@author:NiNi
@deName_List:test
'''
import random
Name_List = "ABCDE"
send = set(Name_List)
rec = set(Name_List)
while send :
    i = random.randint(0,len(Name_List)-1)
    j = random.randint(0,len(Name_List)-1)
    if  Name_List[i] in send and Name_List[j] in rec and i != j:
        print(f"员工{Name_List[i]}送了员工{Name_List[j]}一份礼物")
        send.remove(Name_List[i])
        rec.remove(Name_List[j])
        if len(send) == 2 and len(send&rec) == 1:
            a = ''.join(send & rec)
            b = ''.join(send - rec)
            c = ''.join(rec - send)
            print(f"员工{a}送了员工{b}一份礼物")
            print(f"员工{b}送了员工{c}一份礼物")
            break
