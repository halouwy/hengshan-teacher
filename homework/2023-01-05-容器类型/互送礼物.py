# -*- coding = utf-8 -*-
# @author:zjx
# @time:2023/1/7
# @file:2
# @Software:Pycharm

import random

lst = ['A', 'B', 'C', 'D']
the_rest = lst.copy()       # 收礼人
choose_one = {}             # 送礼品的人和收礼品的人存放在字典
num = len(lst)

for i in range(num):
    lst1 = lst[i]
    lucky_person = ''
    while lucky_person == '' or lucky_person == lst1:    # 确保不会选到自己
        lucky_person = random.choice(the_rest)           # lucky——person用来存放收礼人
    the_rest.remove(lucky_person)                        # 谁收到礼物就删除谁
    choose_one[lst1] = lucky_person                      # 将送礼品的人和收礼品的人存放在字典

for k, v in choose_one.items():
    print(f"{k} --> {v}")
