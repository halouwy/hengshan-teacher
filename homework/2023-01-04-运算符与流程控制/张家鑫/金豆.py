# 初始金豆
import random

temp = 500
while temp > 0:
    # 骰子的点数
    dice_points = random.randint(3, 18)
    guess = input("请猜大还是小: ")
    if guess == "小" and dice_points < 10:
        temp += 100
    elif guess == "大" and dice_points >= 10:
        temp += 100
    else:
        temp -= 100
    print(f"金豆余额{temp}")
    if temp == 0:
        print("金豆余额已归零，游戏结束")
