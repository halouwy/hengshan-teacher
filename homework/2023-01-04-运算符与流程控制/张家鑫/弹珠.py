
print("----------------开始游戏----------------")
while 1:
    # 设置弹珠位置
    sum1 = input("请输入你打中的弹珠位置，只能是1-12。(输入q或Q则退出游戏): ")
    if sum1.lower() == "q":
        break
    elif sum1.isdigit() == False:
        continue
    sum1 = int(sum1)
    if sum1 < 1 or sum1 > 12:
        print("输入错误，请重新输入!")
        continue
    # 亮灯
    temp = random.randint(2048, 4095)
    str1 = bin(temp).split("b")[1]
    print(f"亮灯情况:  {str1}")
    print(f"你打到了第{sum1}个灯")
    # 判断游戏结果
    str2 = str1[::-1]
    sum1 -= 1
    if int(str2[sum1]):
        print("游戏结果 True")
        print()
    else:
        print("游戏结果 False")
        print()

